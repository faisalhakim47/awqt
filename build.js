const child_process = require('child_process')
const fs = require('fs')
const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MinifyPlugin = require('babel-minify-webpack-plugin')
const cssnext = require('postcss-cssnext')

const commands = [
  's',
  'ply',
  'stp',
  'hba',
  'rmd',
  'tst',
  'cmp',
]

child_process.exec('whoami', (error, username) => {
  if (error) {
    console.error(error)
    return
  }
  username = username.slice(0, -1)
  const binPath = path.join(__dirname, './awqt-masjid-api/bin')

  child_process.execSync(`rm -rf ${binPath}`)
  child_process.execSync(`mkdir ${binPath}`)

  const binExport = `export PATH=$PATH:${binPath}`
  const bashrcPath = `/home/${username}/.bashrc`
  const bashrc = fs.readFileSync(bashrcPath, { encoding: 'utf8' })

  const bash = (command) =>
    '#!/bin/bash\n\ncurl localhost/awqt-masjid-api/post.commands.php?name=' + command
  for (const command of commands) {
    fs.writeFileSync(`${binPath}/${command}`, bash(command), { encoding: 'utf8' })
    fs.writeFileSync(`${binPath}/${command.toUpperCase()}`, bash(command), { encoding: 'utf8' })
  }
  child_process.execSync(`chmod +x ${binPath}/*`)

  if (bashrc.indexOf(binExport) === -1) {
    fs.writeFileSync(bashrcPath, bashrc + '\n' + binExport, { encoding: 'utf8' })
  }

  console.log('bin cli installed!')
})

webpack({
  entry: {
    'app': p('./awqt-masjid-app/index.js'),
    'app_style': p('./awqt-masjid-app/index.css.js'),
  },
  output: {
    path: p('./public'),
    filename: '[name].[hash].js',
    chunkFilename: '[name].[hash].chunk.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [{ loader: 'babel-loader' }]
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: [
            {
              loader: 'css-loader',
              options: { minimize: true }
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: [
                  cssnext(),
                ],
              }
            },
          ]
        })
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        loader: 'url-loader',
        options: {
          limit: 100,
        }
      },
    ]
  },
  plugins: [
    new ExtractTextPlugin('app.[hash].css'),
    new MinifyPlugin(),
    new HtmlWebpackPlugin({
      template: p('./index.template.html'),
      filename: 'index.html',
    }),
  ],

  externals: {
    'vue': 'Vue',
    'vue-router': 'VueRouter',
  },
}, (err, stats) => {
  if (err) return console.error(err)
  else console.log(stats.toString())

  link('./static', './public/static')

  link('./awqt-api', './public/awqt-api')
  link('./awqt-masjid-api', './public/awqt-masjid-api')
  link('./awqt-data', './public/awqt-data')

  link('./config.json', './public/config.json')
  link('./.htaccess.prod', './public/.htaccess')
  link('./manifest.json', './public/manifest.json')
})

function link(source, to) {
  fs.unlink(p(to), () => {
    child_process.exec(`ln -s ${p(source)} ${p(to)}`)
  })
}

function p(relativePath) {
  return path.join(__dirname, relativePath)
}
