// @ts-check

const fs = require('fs')
const path = require('path')
const mysql = require('mysql')

const PROVINCE = 'JAWA TENGAH'
const CITY = 'KOTA SEMARANG'

const dir = relPath => path.join(__dirname, relPath)

const pool = mysql.createPool({
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'root',
  database: 'awqt',
  connectionLimit: 10,
})

/**
 * @param {string} sql 
 */
async function executeSql(sql) {
  return await new Promise((resolve, reject) => {
    pool.query({ sql }, (error, results) => {
      if (error) reject(error)
      else resolve(results)
    })
  })
}

/**
 * @param {string} name 
 */
async function ensureTime(name) {
  const times = await executeSql(`
    SELECT id
    FROM times
    WHERE name = '${name}'
    LIMIT 1
  `)
  if (!times.length) {
    const time = await executeSql(`
      INSERT INTO times (name)
      VALUES ('${name}')
    `)
    return time.insertId
  }
  return times[0].id
}

/** @type {{
  name: string
  cities: {
    name: string
    months: {
      index: number
      dates: {
        date: number
        times: {
          name: string
          time: string
        }[]
      }[]
    }[]
  }[]
}[]} */
const provincies = JSON.parse(fs.readFileSync(dir('./kemenag.json'), { encoding: 'utf8' }));

(async () => {
  const months = provincies
    .find((province) => province.name === PROVINCE)
    .cities
    .find((city) => city.name === CITY)
    .months

  for (const month of months) {
    for (const date of month.dates) {
      for (const time of date.times) {
        const time_id = await ensureTime(time.name)
        const [hours, minutes] = time.time.split(':').map((value) => parseInt(value, 10))
        const { insertId: cron_id } = await executeSql(`
          INSERT INTO crons (months, dates, days, hours, minutes, seconds)
          VALUES ('${month.index}', '${date.date}', '*', '${hours}', '${minutes}', '0')
        `)
        await executeSql(`
          INSERT INTO time_crons (time_id, cron_id)
          VALUES (${time_id}, ${cron_id})
        `)
        console.log(cron_id, month.index, date.date, time_id)
      }
    }
  }
})().catch((error) => {
  console.log(error)
})
