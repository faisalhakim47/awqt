const puppeteer = require('puppeteer');
const fs = require('fs');
const path = require('path');

const $PROVINCE = '#opt_lokasi_provinsi';
const $CITY = '#opt_lokasi';

puppeteer.launch().then(async browser => {
  const page = await browser.newPage();

  await page.goto('http://sihat.kemenag.go.id/waktu-sholat');

  const provincies = await page.evaluate(
    (element) => {
      return Array
        .from(element.children)
        .map((option) => option.value)
        .slice(1);
    },
    await page.$($PROVINCE),
  );

  const data = [];

  for (const province of provincies) {
    const prevToken = await page.evaluate(
      ({ children }) => {
        const option = children[1] || children[0];
        return children.length + ':' + option.value;
      },
      await page.$($CITY),
    );

    await page.select($PROVINCE, province);

    await page.waitForFunction(
      ({ children }, { prevToken }) => {
        const option = children[1] || children[0];
        return (children.length + ':' + option.value) !== prevToken;
      },
      {},
      await page.$($CITY),
      { prevToken },
    );

    const cities = await page.evaluate(
      (element) => {
        return Array
          .from(element.children)
          .map((option) => ({ value: option.value, text: option.innerText }))
          .slice(1);
      },
      await page.$($CITY),
    );

    const provinceData = {
      name: province,
      cities: [],
    };

    for (const city of cities) {
      let index = 0;
      const cityData = {
        name: city.text,
        months: [],
      };

      while (++index !== 13) {

        const result = await page.evaluate(async ({ city, index }) => {

          return await new Promise((resolve, reject) => {
            $.post("http://sihat.kemenag.go.id/site/get_waktu_sholat", {
              tahun: 2018,
              bulan: index,
              h: 0,
              lokasi: city.value,
            }, (json) => resolve(JSON.parse(json)));
          });

        }, { city, index });

        cityData.months.push({
          index: index,
          dates: Object.keys(result.data).map((key) => {
            const date = parseInt(key.slice(8), 10)
            const times = result.data[key]
            return {
              date,
              times: Object.keys(times)
                .filter((name) => name !== 'tanggal')
                .map((name) => {
                  return {
                    name,
                    time: times[name],
                  }
                })
            }
          }),
        });

        console.log(city.text, index);
      }

      provinceData.cities.push(cityData);
    }

    data.push(provinceData);
  }


  fs.writeFileSync(path.join(__dirname, 'kemenag.json'), JSON.stringify(data), {
    encoding: 'utf8',
  });

  await browser.close();
});
