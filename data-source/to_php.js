const fs = require('fs')
const path = require('path')
const mkdirp = require('mkdirp')

const dir = relPath => path.join(__dirname, relPath)

const sholatNameMap = [
  { id: 1, key: 'subuh', name: 'Subuh' },
  { id: 2, key: 'dzuhur', name: 'Dzuhur' },
  { id: 3, key: 'ashar', name: 'Ashar' },
  { id: 4, key: 'maghrib', name: 'Maghrib' },
  { id: 5, key: 'isya', name: 'Isya' },
]

/** @type {{
  name: string
  cities: {
    name: string
    months: {
      index: number
      dates: {
        date: number
        times: {
          name: string
          time: string
        }[]
      }[]
    }[]
  }[]
}[]} */
const provincies = JSON.parse(fs.readFileSync(dir('./kemenag.json'), { encoding: 'utf8' }));

(async () => {

  for (const province of provincies) {
    mkdirp.sync(dir(`../awqt-api/imports/${province.name}`))
    for (const city of province.cities) {
      let sql = `<?php

require_once __DIR__."/../../app.php";

$time_ids = [1, 2, 3, 4, 5];
foreach ($time_ids as $time_id) {
  $time_crons = execute_sql("SELECT cron_id FROM time_crons WHERE time_id = {$time_id}")->fetchAll();
  execute_delete_sql("time_crons", ["time_id" => [$time_id, PDO::PARAM_INT]]);
  foreach ($time_crons as $time_cron) {
    execute_delete_sql("crons", ["id" => [$time_cron["cron_id"], PDO::PARAM_INT]]);
  }
}

$actions = [];`

      for (const month of city.months) for (const date of month.dates) for (const time of sholatNameMap) {
        const [hours, minutes] = date.times
          .find((t) => t.name === time.key)
          .time
          .split(':')
          .map((value) => parseInt(value, 10))
        sql += `
array_push($actions, function () {
  $cron_id = execute_insert_sql("crons", [
    "months" => "${month.index}",
    "dates" => "${date.date}",
    "days" => "*",
    "hours" => "${hours}",
    "minutes" => "${minutes}",
    "seconds" => "0",
  ]);
  execute_insert_sql("time_crons", [
    "time_id" => [${time.id}, PDO::PARAM_INT],
    "cron_id" => [$cron_id, PDO::PARAM_INT],
  ]);
});`
      }
      sql += `
foreach ($actions as $action) $action();
send_json(200, ["ok" => true]);
`
      fs.writeFileSync(dir(`../awqt-api/imports/${province.name}/${city.name}.php`), sql, { encoding: 'utf8' })
      console.log(province.name, city.name)
    }
  }

})().catch((error) => {
  console.log(error)
})
