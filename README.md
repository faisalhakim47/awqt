# Awqt

Awqt adalah aplikasi penjadwalan atau timer untuk otomatisasi seluruh perangkat elektronik. Target utama dari aplikasi ini adalah perangkat masjid Al Muttaqin Watusari.

# Struktur aplikasi

## Backend

Sebagai penyimpan user data, aplikasi ini menggunakan database couchdb. Alasanya adalah kemudahan dan fleksibilitas dalam penggunaanya. Dan juga database ini sangat cocok untuk perangkat dengan tenaga rendah seperti Raspberry Pi.

# Frontend

Frontend dalam aplikasi ini menggunakan Vue sebagai framework javascript dan Bulma sebagai framework css-nya.

Strukturnya sangat sederhana, folder `/frontend/app` berisi seluruh code aplikasi dan folder `/frontend/static` berisi code vendor (spt. vue, vue-router, axios, dll)
