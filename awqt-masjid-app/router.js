import Dashboard from './pages/Dashboard.js'
import SchedulePackList from './pages/SchedulePackList.js'
import PlaylistList from './pages/PlaylistList.js'
import Configuration from './pages/Configuration.js'

export const router = new VueRouter({
  mode: 'history',
  routes: [
    { name: 'Dashboard', path: '/', component: Dashboard },
    { name: 'SchedulePackList', path: '/schedule_packs', component: SchedulePackList },
    { name: 'PlaylistList', path: '/playlists', component: PlaylistList },
    { name: 'configuration', path: '/configuration', component: Configuration },
    { path: '*', redirect: { name: 'Dashboard' } },
  ]
})
