import { toDigit } from '../../vue-tampan/src/index.js'

/**
 * @param {number} input 
 */
export function durationFormat(input) {
  const twoDigit = (num) => toDigit(num, 2)
  const milliseconds = input % 1000
  input = (input - milliseconds) / 1000
  const seconds = input % 60
  const minutes = (input - seconds) / 60
  return `${twoDigit(minutes)}:${twoDigit(seconds)}`
}
