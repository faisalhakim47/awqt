const Vue = require('vue').default
import { VueTampan } from '../vue-tampan/src/index.js'
import { router } from './router.js'

Vue.use(VueTampan)

export default new Vue({
  router,

  el: '#app',

  data() {
    return {
      menuGroups: [
        {
          name: 'Jadwal',
          menus: [
            {
              name: 'Beranda',
              route: { name: 'Dashboard' },
              iconText: 'dashboard',
              exact: true,
            },
            {
              name: 'Paket Jadwal',
              route: { name: 'SchedulePackList' },
              iconText: 'event',
            },
            {
              name: 'Playlist',
              route: { name: 'PlaylistList' },
              iconText: 'playlist_play',
            },
            {
              name: 'Konfigurasi',
              route: { name: 'configuration' },
              iconText: 'settings',
            },
          ]
        },
      ],
    }
  },

  template: `
  <admin-panel :menu-groups="menuGroups">
    <div slot="header" class="brand">
      <img class="brand-logo" src="/static/icons/ramadan-sunrise.svg">
      <h1 class="brand-name">Awqt</h1>
    </div>
    <router-view slot="content"></router-view>
  </admin-panel>
  `
})
