import { request } from '../../vue-tampan/src/index.js'
import { objectToQueryString } from './url.js'

const AWQT_SERVER = location.origin + '/awqt-api'

export function constructRequest(method, path, userOptions = {}) {
  const base = userOptions.base || ''
  return {
    url: `${base || AWQT_SERVER}/${method}.${path}.php${userOptions.query ? '?' + objectToQueryString(userOptions.query) : ''}`,
    options: {
      ...userOptions,
      headers: {
        ...(userOptions.headers || {}),
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'App-Name': 'awqt',
        'App-Token': localStorage.getItem('storeman_token'),
      },
    },
  }
}

/**
 * @template Val
 * @param {string} method 
 * @param {string} path 
 * @param {object} options 
 * @returns {Promise<Val>}
 */
export function api(method, path, userOptions = {}) {
  const { url, options } = constructRequest(method, path, userOptions)
  return request('POST', url, options)
    .then(({ data }) => data)
}

/**
 * @template Val
 * @param {string} method 
 * @param {string} path 
 * @param {object} options 
 * @returns {Promise<Val>}
 */
export function masjidApi(method, path, userOptions = {}) {
  const { url, options } = constructRequest(method, path, {
    base: location.origin + '/awqt-masjid-api',
    ...userOptions
  })
  console.log(url)
  return request('POST', url, options)
    .then(({ data }) => data)
}

export function setAPIToken(token) {
  localStorage.setItem('storeman_token', token)
}
