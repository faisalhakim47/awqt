const registeredModules = Object.create(null)

/**
 * @param {string} moduleName 
 */
export function registerModule(moduleName, { moduleLoader }) {
  registeredModules[moduleName] = {
    moduleLoader,
    status: false,
  }
}

/**
 * @param {string} moduleName 
 * @returns {Promise<any>}
 */
export function requireModule(moduleName) {
  return new Promise((resolve, reject) => {
    const theModule = registeredModules[moduleName]

    if (!!theModule && typeof theModule !== 'object') {
      reject('Module has not been registered yet.')
      return
    }

    switch (theModule.status) {
      case 'loading':
        theModule.modulePromise.then(() => {
          resolve(theModule.variable)
        })
        return
      case 'loaded':
        resolve(theModule.variable)
        return
    }

    theModule.status = 'loading'

    theModule.modulePromise = theModule.moduleLoader().then(variable => {
      theModule.variable = variable
      theModule.status = 'loaded'
      resolve(variable)
    })
  })
}

/**
 * @param {string} src 
 * @returns {Promise<void>}
 */
export function loadScript(src) {
  return new Promise((resolve, reject) => {
    const elScript = document.createElement('script')
    elScript.src = src
    elScript.onload = resolve
    elScript.onerror = reject
    document.head.appendChild(elScript)
  })
}

/**
 * @param {string} href 
 * @returns {Promise<void>}
 */
export function loadStyle(href) {
  return new Promise((resolve, reject) => {
    const elStyle = document.createElement('link')
    elStyle.rel = 'stylesheet'
    elStyle.href = href
    elStyle.onload = resolve
    elStyle.onerror = reject
    document.head.appendChild(elStyle)
  })
}
