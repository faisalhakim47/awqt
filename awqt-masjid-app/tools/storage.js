// @ts-check

class Storage {
  constructor({ prefix = '___' }) {
    this.prefix = prefix
  }

  /**
   * @param {string} key 
   */
  get(key) {
    const rawValue = localStorage.getItem(this.prefix + key)
    let value = rawValue
    try {
      value = JSON.parse(value)
    } catch (e) { }
    return Promise.resolve(value)
  }

  /**
   * @param {string} key 
   * @param {any} value 
   */
  set(key, value) {
    if (typeof value === 'object') {
      value = JSON.stringify(value)
    }
    const result = localStorage.setItem(this.prefix + key, value)
    return Promise.resolve(result)
  }

  /**
   * @param {string} key 
   */
  unset(key) {
    const result = localStorage.removeItem(this.prefix + key)
    return Promise.resolve(result)
  }
}

export const storage = new Storage({ prefix: '__Q__' })
