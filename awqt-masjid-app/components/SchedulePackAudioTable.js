const Vue = require('vue').default
import { throttle } from '../../vue-tampan/src/index.js'
import { api } from '../tools/api.js'
import { durationFormat } from '../model/audio.js'

import AudioUpload from './AudioUpload.js'
import ModalAudioPlay from './ModalAudioPlay.js'

export default Vue.extend({
  components: {
    AudioUpload,
    ModalAudioPlay,
  },

  props: {
    schedule_id: { type: Number, required: true },
    schedule_name: { type: String, required: true },
  },

  data() {
    return {
      isOrdering: false,
      isDeleting: false,
      openAudioUpload: false,
      openModalAudioPlay: false,
      box_content: null,
      activeAudioMd5: null,
      audios: [],
    }
  },

  computed: {
    activeAudio() {
      return this.audios.find((audio) => {
        return audio.md5 === this.activeAudioMd5
      })
    },

    sortedAudios() {
      return this.audios.sort((a, b) => a.position - b.position)
    },

    audioControlsStyle() {
      if (this.$tampan.client.isLargeScreen && this.box_content) {
        return {
          width: this.box_content.offsetWidth - 6 + 'px',
          left: this.box_content.offsetLeft - 1 + 'px',
          bottom: '1rem',
          border: '1px solid #E0E0E0',
          boxShadow: '0 4px 5px 0 rgba(0,0,0,0.14), 0 1px 10px 0 rgba(0,0,0,0.12), 0 2px 4px -1px rgba(0,0,0,0.3)',
        }
      }
      return {}
    },

    totalDuration() {
      return this.audios
        .map((audio) => audio.duration)
        .reduce((sum, duration) => sum + duration, 0)
    },
  },

  methods: {
    durationFormat,

    loadAudios() {
      const { schedule_id } = this
      if (!schedule_id) return
      if (typeof schedule_id !== 'number') return
      const loading = api('get', 'schedule_audio-list', { query: { schedule_id } })
        .then((audios) => {
          this.audios = audios
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memuat Audio-audio',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    doneAudioUpload(audios) {
      const saving = Promise
        .all(audios.map((audio) => {
          const data = {
            audio_md5: audio.md5,
            schedule_id: this.schedule_id,
          }
          return api('post', 'schedule_audio-item', { data })
        }))
        .then(this.loadAudios)
        .then(() => api('post', 'compute_next'))
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Mentimpan Audio',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(saving)
    },

    changeOrder(direction) {
      if (this.isOrdering) return
      this.isOrdering = true
      const data = {
        direction,
        audio_md5: this.activeAudio.md5,
        schedule_id: this.schedule_id,
      }
      const ordering = api('patch', 'schedule_audio-item-order', { data })
        .then(this.loadAudios)
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Mengubah Urutan Audio',
            text: error,
          })
          console.warn(error)
        })
        .then(() => this.isOrdering = false)
        .then(() => api('post', 'compute_next'))
      this.$tampan.useLoadingState(ordering)
    },

    deleteAudio() {
      if (this.isDeleting) return

      this.$tampan
        .confirm({
          title: 'Anda yakin menghapus audio ini?',
          confirmText: 'Hapus',
          confirmIconText: 'delete_forever',
        })
        .then(() => {
          this.isDeleting = true
          const data = {
            audio_md5: this.activeAudio.md5,
            schedule_id: this.schedule_id,
          }
          const deleting = api('delete', 'schedule_audio-item', { data })
            .then(this.loadAudios)
            .catch((error) => {
              this.$tampan.alert({
                title: 'Gagal Menghapus Audio',
                text: error,
              })
              console.warn(error)
            })
            .then(() => this.isDeleting = false)
            .then(() => api('post', 'compute_next'))
          this.$tampan.useLoadingState(deleting)
        })

    },
  },

  watch: {
    schedule_id() {
      this.loadAudios()
    },
  },

  mounted() {
    this.loadAudios()
    this.box_content = this.$refs.box_content
  },

  template: `
  <div class="box">
    <div class="box-header" style="display: flex; justify-content: space-between;">
      <h3 class="box-title">Audio-audio</h3>
      <button-tampan icon-text="add" @click="openAudioUpload = true">Audio</button-tampan>
    </div>
    <div ref="box_content" class="box-content">

      <table class="table audios-list borderless ellipsis">
        <tbody>
          <tr v-for="audio in sortedAudios" :key="audio.md5" :class="activeAudioMd5 === audio.md5 && 'active'">
            <td style="text-align: right; width: 1px;">{{ durationFormat(audio.duration) }}</td>
            <td :style="{ 'max-width': $tampan.client.isSmallScreen ? '10rem' : '15rem' }">{{ audio.filename }}</td>
            <td style="text-align: right; width: 1px;">
              <button-tampan v-if="activeAudioMd5 !== audio.md5" icon-text="edit" @click="activeAudioMd5 = audio.md5"></button-tampan>
              <button-tampan v-else icon-text="indeterminate_check_box" @click="activeAudioMd5 = null"></button-tampan>
            </td>
          </tr>
          <tr v-if="!audios.length">
            <td class="emptyinfo" colspan="3">
              Belum ada audio yang ditambahkan. Silahkan upload audio ke jadwal ini.
            </td>
          </tr>
        </tbody>
      </table>

      <p v-if="totalDuration !== 0">total durasi {{ durationFormat(totalDuration) }}</p>

      <div  v-if="activeAudio" class="audio-controls" :style="audioControlsStyle">
        <div class="button-group justify-between">
          <button-tampan
            color="negative outline"
            icon-text="delete_forever"
            :disabled="isDeleting" @click="deleteAudio"
          >Hapus</button-tampan>
          <div class="button-group align-right">
            <button-tampan
              icon-text="play_arrow"
              @click="openModalAudioPlay = true"
            >Play</button-tampan>
            <button-tampan
              icon-text="arrow_downward"
              :disabled="isOrdering"
              @click="changeOrder('down')"
            >Turun</button-tampan>
            <button-tampan
              icon-text="arrow_upward"
              :disabled="isOrdering"
              @click="changeOrder('up')"
            >Naik</button-tampan>
          </div>
        </div>
      </div>

    </div>


    <audio-upload
      :show="openAudioUpload"
      @done="doneAudioUpload"
      @close="openAudioUpload = false"
    ></audio-upload>

    <ModalAudioPlay
      :show="openModalAudioPlay"
      :md5="activeAudioMd5"
      @close="openModalAudioPlay = false"
    ></ModalAudioPlay>
  </div>
  `
})
