const Vue = require('vue').default
import { api } from '../tools/api.js'

export default Vue.extend({
  props: {
    show: { type: Boolean },
    md5: { type: String },
  },

  data() {
    return {
      audio: {}
    }
  },

  methods: {
    loadAudio() {
      if (!this.md5) return
      const loading = api('get', 'audio-item', { query: { md5: this.md5 } })
        .then((audio) => {
          this.audio = audio
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memuat Audio',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },
  },

  watch: {
    md5() {
      this.loadAudio()
    },
  },

  template: `
  <modal :show="show" @close="$emit('close')">
    <h3 slot="header" class="modal-title">{{ audio.filename || '...' }}</h3>

    <audio :src="'/awqt-data/audios/' + md5" controls autoplay style="width: 100%; display: block;"></audio>

    <div slot="footer" class="button-group align-right">
      <button-tampan icon-text="close" @click="$emit('close')">Tutup</button-tampan>
    </div>
  </modal>
  `
})
