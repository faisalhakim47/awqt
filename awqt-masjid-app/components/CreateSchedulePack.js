const Vue = require('vue').default
import { days } from '../../vue-tampan/src/index.js'
import { api } from '../tools/api.js'
import { cron } from '../model/cron.js'

/**
 * @param {string} tagName 
 */
function createSchedulePack(name) {
  const tagName = `:${name}:`
  const sholatTimes = [
    'Subuh',
    'Dzuhur',
    'Ashar',
    'Maghrib',
    'Isya',
  ]
  return sholatTimes
    .reduce((names, sholat, timeIndex) => {
      return names.concat(days.map((day, dayIndex) => ({
        scheduleName: `:${tagName}:${dayIndex + 1}:${timeIndex + 1}:`,
        day: dayIndex + 1,
        time_id: timeIndex + 1,
      })))
    }, [])
    .map(({ scheduleName, day, time_id }) => {
      return {
        name: scheduleName,
        tags: [tagName],
        audios: [],
        include_times: [{ time_id }],
        intersect_crons: [cron('*', '*', day.toString(), '*', '*')],
        exclude_crons: [],
      }
    })
}

export default Vue.extend({
  props: {
    show: { type: Boolean, default: false },
    cancelable: { type: Boolean, default: true },
  },

  data() {
    return {
      pack: {
        name: '',
      },
    }
  },

  methods: {
    saveSchedulePack() {
      const packName = this.pack.name

      if (!packName) return this.$tampan.alert({
        title: 'Kesalahan',
        text: 'Anda belum mengisi nama paket.',
      })

      const tagName = `:${packName}:`
      const saving = api('get', 'tag-item', { query: { name: tagName } })
        .then((tag) => {
          if (tag) return this.$tampan.alert({
            titme: 'Terdapat Kesalahan',
            text: 'Nama paket harus unik. Terdapat paket jadwal lain yang sudah memiliki nama paket tersebut.'
          })

          api('get', 'tag-list', { query: { name: ':%:', is_active: 1 } })
            .then((tags) => !tags.length ? 1 : 0)
            .then((isActive) => {
              return Promise.all(createSchedulePack(packName)
                .map((schedule) => {
                  return api('post', 'schedule-item', { data: schedule })
                }))
                .then(() => isActive)
            })
            .then((is_active) => {
              return api('patch', 'tag-item', { data: { name: tagName, is_active } })
            })
            .then(() => {
              this.$emit('created', { name: tagName })
            })
            .catch((error) => {
              this.$tampan.alert({
                title: 'Gagal Menyimpan',
                text: JSON.stringify(error)
              })
              console.warn(error)
            })
            .then(() => this.pack.name = '')
        })

      this.$tampan.useLoadingState(saving)
    }
  },

  template: `
  <modal :show="show" @close="$emit('close')">
    <h3 slot="header" class="modal-title">Buat Paket Jadwal</h3>
    <field label="Nama Paket">
      <input type="text" v-model="pack.name">
    </field>
    <div slot="footer" class="button-group" :class="cancelable ? 'justify-between' : 'align-right'">
      <button-tampan v-if="cancelable" icon-text="close" @click="$emit('close')">Batal</button-tampan>
      <button-tampan color="positive" icon-text="save" @click="saveSchedulePack">Simpan</button-tampan>
    </div>
  </modal>
  `
})
