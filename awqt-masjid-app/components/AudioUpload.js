const Vue = require('vue').default
import { constructRequest } from '../tools/api.js'

export default Vue.extend({
  props: {
    show: { type: Boolean, default: false },
  },

  data() {
    return {
      audios: [],
    }
  },

  methods: {
    uploadAudios(event) {
      /** @type {File[]} */
      const audios = Array.from(event.target.files)

      this.audios = audios.map((audio) => {
        return {
          filename: audio.name,
          progress: 0,
        }
      })

      Promise
        .all(audios.map((audio, index) => new Promise((resolve, reject) => {
          const { url, options } = constructRequest('post', 'audio-item', {
            query: { filename: audio.name }
          })
          const req = new XMLHttpRequest()
          req.open('POST', url, true)
          Object.keys(options.headers).forEach((header) => {
            req.setRequestHeader(header, options.headers[header])
          })
          req.upload.addEventListener('progress', (progress) => {
            this.audios[index].progress = (progress.loaded / progress.total * 100)
              .toString()
              .slice(0, 5)
          })
          req.addEventListener('error', (error) => {
            reject(error)
          })
          req.addEventListener('loadend', () => {
            this.audios[index].progress = 100
            resolve(JSON.parse(req.responseText))
          })
          req.send(audio)
        })))
        .then((audios) => {
          this.$emit('done', audios)
          this.$emit('close')
          this.audios = []
        })
        .catch((error) => {
          this.$tampan.alert({
            text: 'Gagal Menunggah Audio',
            text: error,
          })
          console.warn(error)
        })
    }
  },

  template: `
  <modal class="audio-upload" :show="show" @close="$emit('close')">
    <h3 slot="header" class="modal-title">Pilih File Audio</h3>

    <p v-if="!audios.length">
      <input id="audio_upload" type="file" accept="audio/*" multiple @change="uploadAudios">
    </p>

    <table v-if="audios.length" class="table">
      <tbody>
        <tr v-for="audio in audios">
          <td>{{ audio.filename }}</td>
          <td>
            <span v-if="audio.progress != 100">{{ audio.progress }}%</span>
            <span v-else class="icon material-icons">check</span>
          </td>
        </tr>
      </tbody>
    </table>

    <div v-if="!audios.length" slot="footer" class="button-group align-right">
      <button-tampan icon-text="close" @click="$emit('close')">Batal</button-tampan>
    </div>
  </modal>
  `
})
