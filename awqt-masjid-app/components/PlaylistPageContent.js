const Vue = require('vue').default
import { api } from '../tools/api.js'
import { durationFormat } from '../model/audio.js'

import AudioUpload from './AudioUpload.js'

export default Vue.extend({
  components: {
    AudioUpload,
  },

  props: {
    id: { type: Number },
  },

  data() {
    return {
      openAudioUpload: false,
      playlist_audios: [],
    }
  },

  methods: {
    durationFormat,

    loadPlaylistAudios() {
      const query = { playlist_id: this.id }
      const loading = api('get', 'audio-list-by-playlist', { query })
        .then((playlist_audios) => {
          this.playlist_audios = playlist_audios
          if (!playlist_audios.length) {
            this.openAudioUpload = true
          }
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memuat Playlist',
            text: error
          })
        })
      this.$tampan.useLoadingState(loading)
    },

    doneAudioUpload(audios) {
      const saving = Promise
        .all(audios.map((audio) =>
          api('post', 'playlist_audio-item', {
            data: {
              playlist_id: this.id,
              audio_md5: audio.md5
            }
          })
        ))
        .then(() => this.loadPlaylistAudios())
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Menyimpan Audio',
            text: error,
          })
        })
      this.$tampan.useLoadingState(saving)
    },
  },

  mounted() {
    this.loadPlaylistAudios()
  },

  template: `
  <div class="playlist-page-content page-content">
    <row>
      <column :width="{ sm: 1, md: 3/5, lg: 1/2 }">
        <div class="box">
          <div class="box-header" style="display: flex; justify-content: space-between;">
            <h3 class="box-title">Audio-audio</h3>
            <button-tampan icon-text="file_upload" @click="openAudioUpload = true">Upload</button-tampan>
          </div>
          <div class="box-content">
            <table class="table hoverable borderless ellipsis">
              <tbody>
                <tr v-for="audio in playlist_audios">
                  <td :style="{ 'max-width': $tampan.client.isSmallScreen ? '10rem' : '15rem' }">{{ audio.filename }}</td>
                  <td>{{ durationFormat(audio.duration) }}</td>
                  <td>
                    <span class="button-group align-right">
                      <button-tampan icon-text="edit" @click="openAudioModal = true"></button-tampan>
                    </span>
                  </td>
                </tr>
                <tr v-if="!playlist_audios.length">
                  <td class="emptyinfo" colspan="3">
                    Belum ada audio dalam playlist. Silahkan upload audio.
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </column>
    </row>

    <audio-upload :show="openAudioUpload" @done="doneAudioUpload" @close="openAudioUpload = false"></audio-upload>
  </div>
  `
})
