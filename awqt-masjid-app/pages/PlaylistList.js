const Vue = require('vue').default
import { cloneObject } from '../../vue-tampan/src/index.js'
import { api } from '../tools/api.js'

import CreatePlaylist from '../components/CreatePlaylist.js'
import PlaylistPageContent from '../components/PlaylistPageContent.js'

export default Vue.extend({
  name: 'PlaylistList',

  components: {
    CreatePlaylist,
    PlaylistPageContent,
  },

  data() {
    return {
      openPlaylistSelector: false,
      openCreatePlaylist: false,
      activePlaylist: null,
      playlist_id: 0,
      playlists: [],
    }
  },

  methods: {
    loadPlaylist() {
      const loading = api('get', 'playlist-list')
        .then((playlists) => {
          this.playlists = playlists
          const activePlaylist = playlists[0]
          if (activePlaylist) this.activePlaylist = activePlaylist
          else this.openPlaylistSelector = true
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Playlist',
            text: JSON.stringify(error)
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    playlistCreated() {
      this.loadPlaylist()
      this.openCreatePlaylist = false
    },

    viewPlaylist(playlist) {
      this.openPlaylistSelector = false
      this.activePlaylist = playlist
    },
  },

  mounted() {
    this.loadPlaylist()
  },

  template: `
  <div class="page">
    <div class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title clickable" style="display: flex; align-items: center;" @click="openPlaylistSelector = true">
        Playlist: {{ activePlaylist ? activePlaylist.name : '...' }}
        <span class="icon material-icons" style="margin: .25rem; font-size: 2rem; color: #FFF;">
          arrow_drop_down
        </span>
      </h2>
    </div>

    <playlist-page-content v-if="activePlaylist" :id="activePlaylist.id"></playlist-page-content>
    <p v-else style="text-align: center;">Tidak ada playlist.</p>

    <modal :show="openPlaylistSelector" @close="openPlaylistSelector = false">
      <h3 slot="header" class="modal-title">Pilih Playlist</h3>
      <table class="table borderless" :class="{ hoverable: !$tampan.client.isMobileOS }">
        <tbody style="user-select: none;">
          <tr v-for="playlist in playlists">
            <td>{{ playlist.name }}</td>
            <td style="width: 3rem;">
              <span class="button-group align-right">
                <button-tampan @click="viewPlaylist(playlist)">Lihat</button-tampan>
              </span>
            </td>
          </tr>
          <tr v-if="!playlists.length">
            <td class="emptyinfo" colspan="2">
              Belum ada playlist. Silahkan buat playlist terlebih dahulu.
            </td>
          </tr>
        </tbody>
      </table>
      <div slot="footer" class="button-group justify-between">
        <button-tampan icon-text="close" @click="openPlaylistSelector = false">Tutup</button-tampan>
        <button-tampan icon-text="add" @click="openCreatePlaylist = true">Buat Playlist</button-tampan>
      </div>
    </modal>

    <create-playlist
      :show="openCreatePlaylist"
      @close="openCreatePlaylist = false"
      @created="playlistCreated"
    ></create-playlist>
  </div>
  `
})
