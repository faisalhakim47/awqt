const Vue = require('vue').default
import { cloneObject } from '../../vue-tampan/src/index.js'
import { api } from '../tools/api.js'

import CreateSchedulePack from '../components/CreateSchedulePack.js'
import SchedulePackPageContent from '../components/SchedulePackPageContent.js'

export default Vue.extend( {
  name: 'SchedulePackList',

  components: {
    CreateSchedulePack,
    SchedulePackPageContent,
  },

  data() {
    return {
      openSchedulePackSelector: false,
      openCreateSchedulePack: false,
      schedulePacks: [],
      activeSchedulePack: null,
      isLoading: true,
    }
  },

  computed: {
    isScheduleExist() {
      if (this.isLoading) return true
      if (this.schedulePacks.length) return true
      return false
    },
  },

  methods: {
    loadSchedulePacks() {
      this.isLoading = true

      const loading = api('get', 'tag-list', { query: { keyword: ':%:' } })
        .then((schedulePacs) => {
          return schedulePacs.map((schedulePack) => {
            return {
              ...schedulePack,
              name: schedulePack.name.slice(1, -1)
            }
          })
        })
        .then((schedulePacks) => {
          this.schedulePacks = schedulePacks
          this.activeSchedulePack = schedulePacks.find((pack) => pack.is_active)
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal',
            text: JSON.stringify(error),
          })
        })
        .then(() => this.isLoading = false)

      return this.$tampan.useLoadingState(loading)
    },

    activateSchedulePack(name) {
      const activating = Promise
        .all(this.schedulePacks
          .map((pack) => `:${pack.name}:`)
          .map((tagName) => {
            return api('patch', 'tag-item', { data: { name: tagName, is_active: 0 } })
          })
        )
        .then(() => {
          const tagName = `:${name}:`
          return api('patch', 'tag-item', { data: { name: tagName, is_active: 1 } })
        })
        .then(() => {
          this.schedulePacks = this.schedulePacks.map((pack) => {
            return {
              ...pack,
              is_active: false,
            }
          })
          const pack = this.schedulePacks.find((pack) => pack.name === name)
          if (pack) {
            pack.is_active = true
            this.viewPack(pack)
          }
        })
        .then(() => api('post', 'compute_next'))
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Mengaktifkan Paket',
            text: error,
          })
        })

      this.$tampan.useLoadingState(activating)
    },

    schedulePackCreated(schedulePack) {
      this.openCreateSchedulePack = false
      this.loadSchedulePacks()
    },

    viewPack(schedulePack) {
      this.openSchedulePackSelector = false
      this.activeSchedulePack = schedulePack
    },
  },

  mounted() {
    this.loadSchedulePacks()
  },

  template: `
  <div id="schedule-pack-list" class="page">
    <div class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title clickable" style="display: flex; align-items: center;" @click="openSchedulePackSelector = true">
        Paket: {{ activeSchedulePack ? activeSchedulePack.name : '...' }}
        <span class="icon material-icons" style="margin: .25rem; font-size: 2rem; color: #FFF;">
          arrow_drop_down
        </span>
      </h2>
    </div>

    <schedule-pack-page-content
      v-if="activeSchedulePack"
      :name="activeSchedulePack.name"
    ></schedule-pack-page-content>

    <modal :show="openSchedulePackSelector" @close="openSchedulePackSelector = false">
      <h3 slot="header" class="modal-title">Pilih Jadwal</h3>
      <table class="table borderless" :class="{ hoverable: !$tampan.client.isMobileOS }">
        <tbody style="user-select: none;">
          <tr v-for="pack in schedulePacks">
            <td>{{ pack.name }}</td>
            <td style="text-align: right;">
              <button-tampan
                :disabled="!!pack.is_active"
                :icon-text="pack.is_active ? 'check' : ''"
                @click="activateSchedulePack(pack.name)"
              >{{ pack.is_active ? '' : 'Aktifkan'}}</button-tampan>
              <button-tampan
                @click="viewPack(pack)"
              >Lihat</button-tampan>
            </td>
          </tr>
          <tr v-if="!schedulePacks.length">
            <td class="emptyinfo" colspan="2">
              Belum ada paket jadwal. Silahkan buat paket jadwal terlebih dahulu.
            </td>
          </tr>
        </tbody>
      </table>
      <div slot="footer" class="button-group justify-between">
        <button-tampan icon-text="close" @click="openSchedulePackSelector = false">Tutup</button-tampan>
        <button-tampan icon-text="add" @click="openCreateSchedulePack = true">Buat Paket Jadwal</button-tampan>
      </div>
    </modal>

    <create-schedule-pack
      :show="!isScheduleExist || openCreateSchedulePack"
      :cancelable="isScheduleExist"
      @created="schedulePackCreated"
      @close="openCreateSchedulePack = false"
    ></create-schedule-pack>
  </div>
  `
})
