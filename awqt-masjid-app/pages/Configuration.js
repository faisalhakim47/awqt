const Vue = require('vue').default
import { toDigit } from '../../vue-tampan/src/index.js'
import { api } from '../tools/api.js'

const JUMAT_TIME = 'JUMAT_TIME'
const JUMAT_TIME_BASE = 'JUMAT_TIME_BASE'
const SPEAKER_PIN = 'SPEAKER_PIN'

export default Vue.extend({
  name: 'Configuration',

  data() {
    return {
      configurations: [],
      JUMAT_TIME: new Date(0, 0, 0, 11, 50),
      JUMAT_TIME_BASE: 'dzuhur',
      SPEAKER_PIN: 4,
      jumatTimeBaseOptions: [
        { value: 'dzuhur', label: 'Mengukuti waktu dzuhur' },
        { value: 'manual', label: 'Tentukan sendiri' },
      ],
    }
  },

  methods: {
    loadConfiguratios() {
      const loading = api('get', 'configuration-list')
        .then((configurations) => {
          this.configurations = configurations
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal memuat konfigurasi',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    saveJumatBase() {
      const hours = toDigit(this.JUMAT_TIME.getHours(), 2)
      const minutes = toDigit(this.JUMAT_TIME.getMinutes(), 2)
      const data = {
        JUMAT_TIME: `${hours}:${minutes}`,
        JUMAT_TIME_BASE: this.JUMAT_TIME_BASE,
      }

      const jumatSchedules = api('get', 'schedule-list', { query: { keyword: '::%::6:2:' } })
      const replaceScheduleIncludeTimes = (schedule_id, time_id) =>
        api('get', 'schedule_include_time-list-by-schedule', { query: { schedule_id } })
          .then((include_times) => {
            const deleting = include_times.map((include_time) => {
              return api('delete', 'schedule_include_time-item', { data: include_time })
            })
            return Promise.all(deleting)
          })
          .then(() => {
            return api('post', 'schedule_include_time-item', {
              data: { schedule_id, time_id }
            })
          })

      const baseSet = this.JUMAT_TIME_BASE === 'manual'
        ? api('get', 'time-item', { query: { name: JUMAT_TIME } })
          .then((time) => {
            if (time) {
              return api('get', 'cron-list-by-time', { query: { time_id: time.id } })
                .then(([cron]) => {
                  cron = {
                    ...cron,
                    hours: hours,
                    minutes: minutes,
                  }
                  return api('patch', 'cron-item', { data: cron })
                })
                .then(() => time)
            }
            else {
              const time = {
                name: JUMAT_TIME
              }
              const cron = {
                months: '*',
                dates: '*',
                days: '6',
                hours: hours,
                minutes: minutes,
                seconds: '0',
              }
              return Promise
                .all([
                  api('post', 'time-item', { data: time }),
                  api('post', 'cron-item', { data: cron }),
                ])
                .then(([time, cron]) => {
                  const time_cron = {
                    time_id: time.id,
                    cron_id: cron.id,
                  }
                  return api('post', 'time_cron-item', { data: time_cron })
                    .then(() => time)
                })
            }
          })
          .then((time) => {
            return jumatSchedules.then((jumatSchedules) => {
              const replacing = jumatSchedules.map((schedule) => {
                return replaceScheduleIncludeTimes(schedule.id, time.id)
              })
              return Promise.all(replacing)
            })
          })
        : jumatSchedules.then((jumatSchedules) => {
          const replacing = jumatSchedules.map((schedule) => {
            return replaceScheduleIncludeTimes(schedule.id, 2 /* DZUHUR */)
          })
          return Promise.all(replacing)
        })

      const saving = baseSet
        .then(() => api('patch', 'configuration-item', { data }))
        .then(() => api('post', 'compute_next'))
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal menyimpan basis hari Jum\'at',
            text: error,
          })
          console.warn(error)
        })
        .then(this.loadConfiguratios)
      this.$tampan.useLoadingState(saving)
    },

    speakerOn() {
      const updating = api('post', 'speaker_on')
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal menghidupkan speaker',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(updating)
    },

    speakerOff() {
      const updating = api('post', 'speaker_off')
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal mematikan speaker',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(updating)
    },

    audioPause() {
      const updating = api('post', 'audio_pause')
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal pause audio',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(updating)
    },

    audioStop() {
      const updating = api('post', 'audio_stop')
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal menghentikan audio',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(updating)
    },
  },

  watch: {
    'configurations'(configurations) {
      configurations.forEach(({ name, value }) => {
        switch (name) {
          case JUMAT_TIME:
            const [hours, minutes] = value.split(':').map((t) => parseInt(t, 10))
            this.JUMAT_TIME = new Date(0, 0, 0, hours, minutes)
            break
          case JUMAT_TIME_BASE:
            this.JUMAT_TIME_BASE = value
            break
        }
      })
    },
  },

  mounted() {
    this.loadConfiguratios()
  },

  template: `
  <div class="page">
    <div class="page-header">
      <button-tampan
        v-if="$tampan.isSidebarToggleable"
        icon-text="menu"
        @click="$tampan.toggleSidebar"
      ></button-tampan>
      <h2 class="page-title">Konfigurasi</h2>
    </div>

    <div class="page-content">
      <row>
        <column :width="{ sm: 1, md: 1/2 }">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hari Jum'at</h3>
            </div>
            <div class="box-content">
              <field label="Basis waktu">
                <input-select :options="jumatTimeBaseOptions" v-model="JUMAT_TIME_BASE" @input="saveJumatBase"></input-select>
              </field>
              <field label="Waktu Jum'at">
                <input-time :disabled="JUMAT_TIME_BASE !== 'manual'" v-model="JUMAT_TIME" @input="saveJumatBase"></input-time>
              </field>
            </div>
          </div>
        </column>
      </row>

      <row>
        <column :width="{ sm: 1, md: 1/2 }">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Speaker</h3>
            </div>
            <div class="box-content">
              <div class="button-group">
                <button-tampan color="outline" icon-text="volume_up" @click="speakerOn">Hidupkan</button-tampan>
                <button-tampan color="outline" icon-text="volume_off" @click="speakerOff">Matikan</button-tampan>
              </div>
            </div>
          </div>
        </column>
      </row>

      <row>
        <column :width="{ sm: 1, md: 1/2 }">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Audio</h3>
            </div>
            <div class="box-content">
              <div class="button-group">
                <button-tampan color="outline" icon-text="pause" @click="audioPause">Pause</button-tampan>
                <button-tampan color="outline" icon-text="stop" @click="audioStop">Stop</button-tampan>
              </div>
            </div>
          </div>
        </column>
      </row>

    </div>
  </div>
  `
})
