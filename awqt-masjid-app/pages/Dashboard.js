const Vue = require('vue').default
import { toDigit } from '../../vue-tampan/src/index.js'
import { api, masjidApi } from '../tools/api.js'
import { durationFormat } from '../model/audio.js'

export default Vue.extend({
  name: 'Dashboard',

  data() {
    return {
      activeTimers: [],
      audios: [],
      times: [],
      time_left: 0,
      timezone: new Date().getTimezoneOffset() / 60,
      serverTime: 0,
    }
  },

  computed: {
    time() {
      if (this.time_left < 0) return '00:00:00'
      const date = new Date(this.time_left)
      date.setHours(date.getHours() + this.timezone)
      return `${toDigit(date.getHours(), 2)}:${toDigit(date.getMinutes(), 2)}:${toDigit(date.getSeconds(), 2)}`
    },
    serverTimeString() {
      const date = new Date(this.serverTime)
      return `${toDigit(date.getHours(), 2)}:${toDigit(date.getMinutes(), 2)}:${toDigit(date.getSeconds(), 2)}`
    },
    nextTime() {
      return this.times.find((time) => {
        const date = new Date()
        date.setHours(parseInt(time.hours, 10))
        date.setMinutes(parseInt(time.minutes, 10))
        return date.getTime() > new Date().getTime()
      })
    },
    totalAudios() {
      return durationFormat(
        this.audios
          .map((audio) => {
            return audio.duration
          })
          .reduce((sum, duration) => {
            return sum + duration
          }, 0)
      )
    },
  },

  methods: {
    durationFormat,
    toDigit,

    loadActiveTimers() {
      const loading = api('get', 'active_job-list')
        .then((data) => {
          this.activeTimers = data.result
          const activeTimer = this.activeTimers.find(({ name }) => {
            return name === 'AUDIO_PLAY_WITH_SPEAKER' || name === 'AUDIO_PLAY'
          })
          if (activeTimer) {
            this.time_left = new Date(activeTimer.time).getTime() - new Date().getTime()
          }
          const audios = this.activeTimers.find(({ name }) => {
            return name === 'AUDIO_PLAY'
          })
          if (audios) {
            Promise.all(
              audios.pyld.audio_files
                .map((audioFile) => {
                  const audioPaths = audioFile.split('/')
                  return audioPaths[audioPaths.length - 1]
                })
                .map((md5) => {
                  return api('get', 'audio-item', { query: { md5 } })
                })
            ).then((audios) => {
              this.audios = audios
            })
          }
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Timer',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    loadServerTime() {
      const loading = api('get', 'server_time')
        .then((server_time) => {
          this.serverTime = new Date(server_time.time).getTime()
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Waktu Server',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },

    loadTimes() {
      const now = new Date()
      const query = {
        months: now.getMonth() + 1,
        dates: now.getDate(),
      }
      const loading = masjidApi('get', 'time-list-by-date', { query })
        .then((times) => {
          this.times = times
        })
        .catch((error) => {
          this.$tampan.alert({
            title: 'Gagal Memulat Waktu Sholat',
            text: error,
          })
          console.warn(error)
        })
      this.$tampan.useLoadingState(loading)
    },
  },

  mounted() {
    this.loadActiveTimers()
    this.loadServerTime()
    this.loadTimes()
    this.animatingTime = true
    const now = new Date().getTime()
    let prevTime = performance.now()
    const animateTime = (time) => {
      const diff = time - prevTime
      this.time_left -= diff
      this.serverTime += diff
      prevTime = time
      if (this.animatingTime) requestAnimationFrame(animateTime)
    }
    requestAnimationFrame(animateTime)
  },

  beforeDestroy() {
    this.animatingTime = false
  },

  template: `
  <div class="page">
    <div class="page-content">
      <p style="
        margin: 2rem;
        text-align: center;
      ">
        <img style="width: 200px;" src="/static/icons/ramadan-sunrise-orange-ubuntu.svg">
      </p>
      <h3 style="
        font-size: 1.6rem;
        font-weight: 400;
        text-align: center;
      ">Jadwal Selanjutnya</h3>
      <p style="
        margin: 0rem;
        text-align: center;
        font-size: 4rem;
        font-weight: 300;
        font-variant-numeric: tabular-nums;
      ">
        {{ time }}
      </p>
      <p style="
        margin: 0 auto 2rem auto;
        text-align: center;
        font-size: 1.5rem;
        font-weight: 300;
        font-variant-numeric: tabular-nums;
      ">
        waktu saat ini {{ serverTimeString }}
      </p>
      <section class="box">
        <div class="box-content">
          <table class="table borderless">
            <thead>
              <tr>
                <th colspan="2">Jadwal Sholat Hari Ini</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="time in times" :key="time.time_name" :style="nextTime === time ? 'font-weight: 500;' : ''">
                <td>{{ time.time_name }}</td>
                <td>{{ toDigit(time.hours, 2) }}:{{ toDigit(time.minutes, 2) }}</td>
              </tr>
            </tbody>
          </table>
        </div>
      </section>
      <section class="box">
        <div class="box-content">
          <table class="table borderless">
            <thead>
              <tr>
                <th>Materi Selanjutnya</th>
                <th>Durasi</th>
              </tr>
            </thead>
            <tbody>
              <tr v-for="audio in audios" :key="audio.md5">
                <td>{{ audio.filename }}</td>
                <td>{{ durationFormat(audio.duration) }}</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td style="text-align: right;">total</td>
                <td style="font-weight: 500;">{{ totalAudios }}</td>
              </tr>
            </tfoot>
          </table>
        </div>
      </section>
      <div class="button-group align-center">
        <button-tampan
          icon-text="settings"
          @click="$router.push({ name: 'SchedulePackList' })"
        >Edit Paket Jadwal</button-tampan>
      </div>
    </div>
  </div>
  `
})
