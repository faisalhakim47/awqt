from threading import Timer
import subprocess

PLAYER = None
IS_TERMINATED = False


def audio_play(audio_files, seek_skip):
    global PLAYER
    global IS_TERMINATED
    audio_stop()
    IS_TERMINATED = False
    for audio_file in audio_files:
        print('AUDIO_PLAY_BEGIN', seek_skip, audio_file)
        PLAYER = subprocess.Popen(
            ['mplayer', '-ss', str(seek_skip), audio_file])
        PLAYER.wait()
        print('AUDIO_PLAY_END', IS_TERMINATED, audio_file)
        seek_skip = 0
        if IS_TERMINATED: break
    audio_stop()
    IS_TERMINATED = False


def audio_pause():
    global PLAYER
    if PLAYER == None: return
    PLAYER.stdin.write(b'p')
    print('AUDIO PAUSE')


def audio_stop():
    global PLAYER
    global IS_TERMINATED
    if PLAYER == None: return
    IS_TERMINATED = True
    try:
        PLAYER.terminate()
    except Exception:
        pass
    PLAYER = None
    print('AUDIO STOPED')
