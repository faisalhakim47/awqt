PINS = []
ACTIVE_PINS = []


def ensure_pin(pin):
    if pin in PINS:
        return
    else:
        PINS.append(pin)


def get_status(pin):
    ensure_pin(pin)
    return pin in ACTIVE_PINS


def turn_on(pin):
    ensure_pin(pin)
    ACTIVE_PINS.append(pin)
    print('Turn ON pin: ', pin)


def turn_off(pin):
    ensure_pin(pin)
    if pin in ACTIVE_PINS: ACTIVE_PINS.remove(pin)
    print('Turn OFF pin: ', pin)


def toggle_relay(pin):
    if get_status(pin): turn_off(pin)
    else: turn_on(pin)
