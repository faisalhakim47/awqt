from RPi import GPIO

GPIO.setmode(GPIO.BCM)

PINS = []


def ensure_pin(pin):
    if pin in PINS: return
    else:
        GPIO.setup(pin, GPIO.IN)
        GPIO.setup(pin, GPIO.OUT)
        PINS.append(pin)


def get_status(pin):
    ensure_pin(pin)
    return GPIO.input(pin)


def turn_on(pin):
    ensure_pin(pin)
    return GPIO.output(pin, GPIO.LOW)


def turn_off(pin):
    ensure_pin(pin)
    return GPIO.output(pin, GPIO.HIGH)


def toggle_relay(pin):
    if get_status(pin) == 1:
        return turn_on(pin)
    else:
        return turn_off(pin)
