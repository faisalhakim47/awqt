import mysql.connector
from config import config

mysql_connection = mysql.connector.connect(
    user=config.get('DB_USER'),
    password=config.get('DB_PASS'),
    host=config.get('DB_HOST'),
    database=config.get('DB_NAME'))


def execute_sql(query):
    cursor = mysql_connection.cursor(buffered=True)
    cursor.execute(query)
    return cursor


def commit_change():
    mysql_connection.commit()
