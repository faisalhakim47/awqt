from datetime import datetime, timedelta
from os import path

from jobs import SPEAKER_ON, SPEAKER_OFF, AUDIO_PLAY, SCHEDULE_NEXT, COMPUTE_NEXT

from model.audio import audio_path
from model.cron import cron_time_intersect, compute_cron
from tools import media
from tools.mysql import execute_sql, commit_change
from timer import JobSchedule, datetime_to_second_left

ACTIVE_SCHEDULE_TIMERS = []
ACTIVE_SCHEDULE_AUDIOS = []


def schedule_test():
    AUDIO_NAME = 'data/bismillah.m4a'
    AUDIO_FILE = path.join(path.dirname(path.abspath(__file__)), AUDIO_NAME)
    JobSchedule(0, SPEAKER_ON)
    print(AUDIO_FILE)
    JobSchedule(
        3, AUDIO_PLAY, {
            'audio_names': [AUDIO_NAME, AUDIO_NAME],
            'audio_files': [AUDIO_FILE, AUDIO_FILE],
            'seek_skip': 0,
        })
    JobSchedule(15, SPEAKER_OFF)


def schedule_next(now=False):
    global ACTIVE_SCHEDULE_TIMERS
    global ACTIVE_SCHEDULE_AUDIOS

    JobSchedule(0, SPEAKER_OFF)

    if now == False: now = datetime.now()

    for job_schedule in ACTIVE_SCHEDULE_TIMERS:
        job_schedule.cancel()
    ACTIVE_SCHEDULE_TIMERS = []
    ACTIVE_SCHEDULE_AUDIOS = []

    print('\nScheduling begin at {0}... '.format(now))

    time = execute_sql("""
        SELECT
            time_crons.time_id AS time_id,
            computed_crons.time AS time
        FROM computed_crons
        JOIN time_crons ON time_crons.cron_id = computed_crons.cron_id
        WHERE time > '{0}'
        ORDER BY computed_crons.time
        LIMIT 1
    """.format(now)).fetchone()

    if time == None:
        print(' === === === SCHEDULING ABORT === === === ')
        return

    audio_files = []
    audio_names = []
    registered_schedules = []

    delta_second = datetime_to_second_left(time[1])

    schedule_ids = execute_sql("""
        SELECT
            schedule_include_times.schedule_id AS schedule_id,
            schedules.name AS schedule_name
        FROM schedule_include_times
        JOIN schedule_tags ON schedule_tags.schedule_id = schedule_include_times.schedule_id
        JOIN schedules ON schedules.id = schedule_include_times.schedule_id
        JOIN tags ON tags.name = schedule_tags.tag_name
        WHERE schedule_include_times.time_id = {0} AND tags.is_active = 1
    """.format(time[0])).fetchall()

    for schedule_id, schedule_name in schedule_ids:
        is_skip = False
        intersections = execute_sql("""
            SELECT
                months,
                dates,
                days,
                hours,
                minutes,
                seconds
            FROM schedule_intersect_crons
            WHERE schedule_id = {0}
        """.format(schedule_id)).fetchall()
        for intersection in intersections:
            if cron_time_intersect(time[1], intersection) == False:
                is_skip = True

        if is_skip: continue

        audios = execute_sql("""
            SELECT
                schedule_audios.audio_md5 AS audio_md5,
                audios.filename AS filename,
                audios.duration AS duration
            FROM schedule_audios
            JOIN audios ON audios.md5 = schedule_audios.audio_md5
            WHERE schedule_audios.schedule_id = {0}
            ORDER BY schedule_audios.position ASC
        """.format(schedule_id)).fetchall()

        total_duration = 0
        delta_millisecond = delta_second * 1000
        for audio_md5, filename, duration in reversed(audios):
            if total_duration > delta_millisecond: break
            total_duration += duration
            audio_file = audio_path(audio_md5)
            audio_files.append(audio_file)
            audio_names.append(filename)
        audio_files = list(reversed(audio_files))
        audio_names = list(reversed(audio_names))

        # convert from millisecond to second
        total_duration = total_duration / 1000

        seek_skip = 0
        speaker_on_time = delta_second - total_duration - 3
        audio_play_time = delta_second - total_duration
        speaker_off_time = delta_second + 3
        schedule_next_time = delta_second + 5

        if audio_play_time < 0:
            seek_skip = abs(audio_play_time)
            speaker_on_time = 0
            audio_play_time = 0

        ACTIVE_SCHEDULE_TIMERS.append(JobSchedule(speaker_on_time, SPEAKER_ON))

        ACTIVE_SCHEDULE_TIMERS.append(
            JobSchedule(
                audio_play_time, AUDIO_PLAY, {
                    'audio_names': audio_names,
                    'audio_files': audio_files,
                    'seek_skip': seek_skip,
                }))

        ACTIVE_SCHEDULE_TIMERS.append(
            JobSchedule(speaker_off_time, SPEAKER_OFF))

        registered_schedules.append(schedule_name)

    if len(registered_schedules) == 0:
        return schedule_next(time[1])

    print('\nRegistered schedules: ', registered_schedules)

    print('Registered audios: ', audio_names, '\n')
    for audio_name in audio_names:
        ACTIVE_SCHEDULE_AUDIOS.append(audio_name)

    ACTIVE_SCHEDULE_TIMERS.append(
        JobSchedule(schedule_next_time, SCHEDULE_NEXT))

    print('\nscheduling done.\n')
    return {'audio_names': audio_names, 'time': time[1]}


ACTIVE_COMPUTE_TIMER = None


def compute_next(hour=0):
    global ACTIVE_COMPUTE_TIMER

    print('\ncompute begin at {0} ...'.format(datetime.now()))

    if ACTIVE_COMPUTE_TIMER != None:
        ACTIVE_COMPUTE_TIMER.cancel()

    JobSchedule(0, SPEAKER_OFF)

    now = datetime.now()
    currtime = datetime.now()
    nexttime = datetime.now() + timedelta(days=1)
    nexttime = nexttime.replace(hour=hour, minute=0, second=0)
    execute_sql('DELETE FROM computed_crons')
    crons = execute_sql("""
        SELECT
            id,
            months,
            dates,
            days,
            hours,
            minutes,
            seconds
        FROM crons
    """)

    commit_change()

    for cron_id, months, dates, days, hours, minutes, seconds in crons:
        computed_crons = compute_cron({
            'id': cron_id,
            'months': months,
            'dates': dates,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds,
        })
        for computed_cron in computed_crons:
            if computed_cron['time'] > currtime and computed_cron['time'] < nexttime:
                execute_sql("""
                    INSERT INTO computed_crons (cron_id, time) VALUES ({0}, '{1}')
                """.format(computed_cron['cron_id'], computed_cron['time']))

    commit_change()

    schedule_next()

    ACTIVE_COMPUTE_TIMER = JobSchedule(
        datetime_to_second_left(nexttime), COMPUTE_NEXT)

    print('\ncompute done.\n')


compute_next()
