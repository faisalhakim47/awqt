import os


def audio_path(audio_md5):
    return os.path.join(
        os.path.dirname(__file__), '../../awqt-data/audios', audio_md5)
