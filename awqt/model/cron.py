from datetime import datetime
from calendar import monthrange


def datetime_to_cron(dt):
    return {months: str(dt.month), dates: str(dt.day), days: '*'}


def cron_diff(cron1, cron2):
    return True


def cron_time_intersect(datetime, cron):
    weekday = datetime.isoweekday() + 1
    if weekday == 8:
        weekday = 1
    cron_time = [
        str(datetime.month),
        str(datetime.day),
        str(weekday),
        str(datetime.hour),
        str(datetime.minute),
        str(datetime.second)
    ]

    def part_intersect(time, cron):
        if cron == '*':
            return True
        if time in list(map(str, split_to_number(cron))):
            return True
        return False

    for index, time in enumerate(cron_time):
        if part_intersect(time, cron[index]) == False:
            return False
    return True


def split_to_number(string):
    return list(map(int, string.split(',')))


def time_part(part, all):
    return all if part == '*' else split_to_number(part)


def compute_cron(cron):
    now = datetime.now()
    year = now.year
    months = time_part(cron['months'], range(1, 13))
    days = time_part(cron['days'], range(1, 8))
    hours = time_part(cron['hours'], range(0, 24))
    minutes = time_part(cron['minutes'], range(0, 60))
    seconds = time_part(cron['seconds'], range(0, 60))

    def day_of_week(month, date):
        weekday = datetime(year, month, date).isoweekday() + 1
        return 0 if weekday == 8 else weekday

    computed_crons = []

    for month in months:
        dates = time_part(cron['dates'],
                          list(range(1,
                                     monthrange(year, month)[1] + 1)))
        if cron['days'] != '*':
            dates = filter(lambda date: day_of_week(month, date) in days,
                           dates)
        for date in dates:
            for hour in hours:
                for minute in minutes:
                    for second in seconds:
                        time = datetime(year, month, date, hour, minute,
                                        second)
                        computed_crons.append({
                            'cron_id': cron['id'],
                            'time': time,
                        })

    return computed_crons
