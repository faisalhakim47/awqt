import json
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qsl

from tools.media import audio_pause, audio_stop
from schedule import schedule_test
from timer import JobSchedule, get_active_jobs

from tools.pi import ispi
if ispi():
    from tools.relay_device import turn_on, turn_off, get_status, toggle_relay
else:
    from tools.relay_device_fake import turn_on, turn_off, get_status, toggle_relay


class HTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.parse_req()

        if self.path_parts[1] == '':
            self.send_json(200, {'app_name': 'awqt', 'version': '1'})

        elif self.path_parts[1] == 'schedule_test':
            schedule_test()
            self.send_json(201, {'msg': 'command received'})

        elif self.path_parts[1] == 'schedule_next':
            self.send_json(201, {'msg': 'command received'})
            JobSchedule(0, 'SCHEDULE_NEXT')

        elif self.path_parts[1] == 'compute_next':
            self.send_json(201, {'msg': 'command received'})
            JobSchedule(0, 'COMPUTE_NEXT')

        elif self.path_parts[1] == 'active_jobs':
            active_jobs = []
            for job_schedule in get_active_jobs():
                active_jobs.append({
                    'time': job_schedule.formated_time,
                    'name': job_schedule.name,
                    'pyld': job_schedule.payload,
                })
            self.send_json(200, active_jobs)

        elif self.path_parts[1] == 'audio_pause':
            self.send_json(200, {'ok': True})
            audio_pause()

        elif self.path_parts[1] == 'audio_stop':
            self.send_json(201, {'msg': 'command received'})
            audio_stop()

        elif self.path_parts[1] == 'speaker_on':
            JobSchedule(0, 'SPEAKER_ON')
            self.send_json(201, {'msg': 'command received'})

        elif self.path_parts[1] == 'speaker_off':
            JobSchedule(0, 'SPEAKER_OFF')
            self.send_json(201, {'msg': 'command received'})

        elif self.path_parts[1] == 'speaker_toggle':
            JobSchedule(0, 'SPEAKER_TOGGLE')
            self.send_json(201, {'msg': 'command received'})

        elif self.path_parts[1] == 'pin':
            pin = int(self.path_parts[2])
            if self.path_parts[3] == 'on': turn_on(pin)
            elif self.path_parts[3] == 'off': turn_off(pin)
            elif self.path_parts[3] == 'status': get_status(pin)
            elif self.path_parts[3] == 'toggle': toggle_relay(pin)
            self.send_json(201, {'msg': 'command received'})

        else:
            self.send_json(404, {'msg': 'Not found.'})

    def parse_req(self):
        self.url = urlparse(self.path)
        self.path_parts = self.url.path.split('/')
        self.queries = dict(parse_qsl(self.url.query))

    def query(self, name):
        if name not in self.queries:
            return False
        return self.queries[name]

    def send_json(self, status, obj):
        self.send_response(status)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(
            bytes(json.dumps(obj, sort_keys=True, indent=2), 'utf8'))


httpd = HTTPServer(('127.0.0.1', 5000), HTTPRequestHandler)
print('\nawqt is running @ 127.0.0.1:5000')
httpd.serve_forever()
