import time

import schedule
from config import config
from tools import media
from tools.pi import ispi

if ispi(): from tools.relay_device import turn_on, turn_off, toggle_relay
else: from tools.relay_device_fake import turn_on, turn_off, toggle_relay

SPEAKER_PINS = config.get('SPEAKER_PINS')

SPEAKER_ON = 'SPEAKER_ON'
SPEAKER_OFF = 'SPEAKER_OFF'
SPEAKER_TOGGLE = 'SPEAKER_TOGGLE'
AUDIO_PLAY = 'AUDIO_PLAY'
AUDIO_PLAY_WITH_SPEAKER = 'AUDIO_PLAY_WITH_SPEAKER'
SCHEDULE_NEXT = 'SCHEDULE_NEXT'
COMPUTE_NEXT = 'COMPUTE_NEXT'


def speaker_on(data):
    PINS = SPEAKER_PINS
    for PIN in PINS:
        turn_on(PIN)


def speaker_off(data):
    PINS = SPEAKER_PINS
    for PIN in PINS:
        turn_off(PIN)


def speaker_toggle(data):
    PINS = SPEAKER_PINS
    for PIN in PINS:
        toggle_relay(PIN)


def audio_play(data):
    media.audio_play(data['audio_files'], data['seek_skip'])


def audio_play_with_speaker(data):
    speaker_on(data['pins'])
    time.sleep(3)
    audio_play(data)
    time.sleep(3)
    speaker_off(data['pins'])


def schedule_next(data):
    schedule.schedule_next()


def compute_next(data):
    schedule.compute_next()


available_jobs = {
    SPEAKER_ON: speaker_on,
    SPEAKER_OFF: speaker_off,
    SPEAKER_TOGGLE: speaker_toggle,
    AUDIO_PLAY: audio_play,
    AUDIO_PLAY_WITH_SPEAKER: audio_play_with_speaker,
    SCHEDULE_NEXT: schedule_next,
    COMPUTE_NEXT: compute_next,
}
