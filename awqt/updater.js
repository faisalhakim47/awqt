const path = require('path')

const pm2 = require('pm2')
const git = require('simple-git')

git(path.join(__dirname, '../'))
  .pull(function (result) {
    console.log(result)
  })

pm2.connect((error) => {
  if (error) {
    console.error(error)
    return
  }

  pm2.list((error, apps) => {
    if (error) {
      console.error(error)
      return
    }

    const awqt = apps.find((app) => {
      return app.name === 'awqt'
    })

    pm2.restart('awqt', (error, process) => {
      if (error) {
        console.error(error)
        return
      }

      console.log(process)
    })
  })
})
