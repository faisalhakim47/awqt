from threading import Timer
from datetime import datetime, timedelta
from jobs import available_jobs

ACTIVE_JOB_SCHEDULES = []


def get_active_jobs():
    global ACTIVE_JOB_SCHEDULES
    return ACTIVE_JOB_SCHEDULES


def datetime_to_second_left(dt):
    delta = dt - datetime.now()
    delta_millisecond = (delta.seconds * 1000) + (delta.microseconds / 1000)
    delta_second = delta_millisecond / 1000
    return delta_second


class JobSchedule:
    def __init__(self, time, name, payload=False):
        global ACTIVE_JOB_SCHEDULES
        self.time = time
        self.name = name
        self.payload = payload
        self.formated_time = (datetime.now() + timedelta(0, time)).isoformat()
        if self.time == 0:
            print('Instant timer: ', datetime.now().isoformat(), self.name)
            self.timer = Timer(0, self._execute_job)
            self.timer.start()
        else:
            print('Registered timer: ', self.formated_time, self.name)
            self.timer = Timer(self.time, self._execute_job)
            self.timer.start()
            ACTIVE_JOB_SCHEDULES.append(self)

    def cancel(self):
        if self.time != 0:
            print('Canceled timer: ', self.formated_time, self.name)
            self.timer.cancel()
            if self in ACTIVE_JOB_SCHEDULES:
                ACTIVE_JOB_SCHEDULES.remove(self)

    def _execute_job(self):
        global ACTIVE_JOB_SCHEDULES
        if self.time != 0:
            ACTIVE_JOB_SCHEDULES.remove(self)
        print('Execute timer: ', datetime.now().isoformat(), self.name)
        available_jobs[self.name](self.payload)
