<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/model/schedule_audio.php";

$data = require_json_data();

$subject = execute_sql("
  SELECT
    audio_md5,
    schedule_id,
    position
  FROM schedule_audios
  WHERE schedule_id = :schedule_id AND audio_md5 = :audio_md5
", [
  ":schedule_id" => [$data["schedule_id"], PDO::PARAM_INT],
  ":audio_md5" => [$data["audio_md5"], PDO::PARAM_STR],
])->fetch();

if (!$subject) {
  send_json(400, ["msg" => "Schedule audio not found."]);
}

$comparator = $data["direction"] === 'up' ? '<' : '>';
$order_direction = $data["direction"] === 'up' ? 'DESC' : 'ASC';

$object = execute_sql("
  SELECT
    audio_md5,
    schedule_id,
    position
  FROM schedule_audios
  WHERE schedule_id = :schedule_id AND position $comparator :position
  ORDER BY position $order_direction
  LIMIT 1
", [
  ":schedule_id" => [$subject["schedule_id"], PDO::PARAM_INT],
  ":position" => [$subject["position"], PDO::PARAM_INT],
])->fetch();

if (!$object) {
  send_json(200, ["ok" => true, "on_the_edge" => true]);
}

execute_update_sql("schedule_audios", [
  "position" => $object["position"],
], [
  "schedule_id" => $subject["schedule_id"],
  "audio_md5" => $subject["audio_md5"],
]);

execute_update_sql("schedule_audios", [
  "position" => $subject["position"],
], [
  "schedule_id" => $object["schedule_id"],
  "audio_md5" => $object["audio_md5"],
]);

send_json(200, ["ok" => true]);
