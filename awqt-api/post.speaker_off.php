<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/model/awqt.php";

$result = speaker_off();

send_json(200, [
  "ok" => true,
  "result" => $result,
]);
