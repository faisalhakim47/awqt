<?php

$korcab_attributes = [
  ["label" => "Tashih", "value" => "Tashih"],
  ["label" => "Metodologi", "value" => "Metodologi"],
  ["label" => "Sekretaris", "value" => "Sekretaris"],
  ["label" => "Buku", "value" => "Buku"],
  ["label" => "Tim Penguji", "value" => "Tim Penguji"],
  ["label" => "Pengurus Harian", "value" => "PH"],
  ["label" => "Pelaksana EBTAQ", "value" => "Pelaksana EBTAQ"],
  ["label" => "Tim Penguji Fashohah", "value" => "Tim Penguji Fashohah"],
  ["label" => "Tim Penguji Tartil", "value" => "Tim Penguji Tartil"],
  ["label" => "Tim Penguji Tajwid", "value" => "Tim Penguji Tajwid"],
  ["label" => "Tim Penguji Gharib", "value" => "Tim Penguji Gharib"],
  ["label" => "Tim Penguji Wudhu", "value" => "Tim Penguji Wudhu"],
  ["label" => "Tim Penguji Shalat", "value" => "Tim Penguji Shalat"],
  ["label" => "Tim Penguji Hafalan Surat", "value" => "Tim Penguji Surat"],
  ["label" => "Tim Penguji Hafalan Doa", "value" => "Tim Penguji Doa"],
  ["label" => "Pelaksana MMQ Cabang", "value" => "Pelaksana MMQ Cabang"],
];

$korcam_attributes = [
  ["label" => "Pra Tashih", "value" => "Pra Tashih"],
  ["label" => "Metodologi", "value" => "Metodologi"],
  ["label" => "Sekretaris", "value" => "Sekretaris"],
  ["label" => "Buku", "value" => "Buku"],
  ["label" => "Tim Penguji", "value" => "Tim Penguji"],
  ["label" => "MMQ", "value" => "MMQ"],
  ["label" => "Pra EBTAQ", "value" => "Pra EBTAQ"],
];

$tpq_attributes = [
  ["label" => "Kepala", "value" => "Kepala"],
  ["label" => "Sekretaris", "value" => "Sekretaris"],
  ["label" => "Bendahara", "value" => "Bendahara"],
  ["label" => "Ustadz(ah)", "value" => "Ustadz(ah)"],
];

$structure_levels = [
  ["label" => "Korcab", "value" => "Korcab"],
  ["label" => "Korcam", "value" => "Korcam"],
  ["label" => "Lembaga", "value" => "Lembaga"],
];

$audience_types = [
  ["label" => "Semua", "value" => "Semua"],
  ["label" => "Bersyahadah", "value" => "Bersyahadah"],
  ["label" => "Tidak Bersyahadah", "value" => "Tidak Bersyahadah"],
];

$attendance_types = [
  ["label" => "Hadir", "value" => "Hadir"],
  ["label" => "Ijin", "value" => "Ijin"],
  ["label" => "Terlambat", "value" => "Terlambat"],
  ["label" => "Tidak Hadir", "value" => "Tidak Hadir"],
];
