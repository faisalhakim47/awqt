<?php

require_once __DIR__ . "/app.php";

$prepare = get_query([
  "playlist_id" => [
    "sql_query" => "playlist_audios.playlist_id = :playlist_id",
    "param_type" => PDO::PARAM_INT,
    "required" => true,
  ],
]);

$result = execute_sql("
  SELECT
    audios.filename AS filename,
    audios.filetype AS filetype,
    audios.duration AS duration
  FROM audios
  JOIN playlist_audios ON playlist_audios.audio_md5 = audios.md5
  {$prepare["sql_query"]}
", $prepare["params"])->fetchAll();

send_json(200, $result);
