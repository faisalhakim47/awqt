<?php

require_once __DIR__ . "/app.php";

$cron = require_json_data();

$result = execute_update_sql("crons", [
  "months" => [$cron["months"], PDO::PARAM_STR],
  "dates" => [$cron["dates"], PDO::PARAM_STR],
  "days" => [$cron["days"], PDO::PARAM_STR],
  "hours" => [$cron["hours"], PDO::PARAM_STR],
  "minutes" => [$cron["minutes"], PDO::PARAM_STR],
  "seconds" => [$cron["seconds"], PDO::PARAM_STR],
], [
  "id" => [$cron["id"], PDO::PARAM_INT],
]);

send_json(200, $result);
