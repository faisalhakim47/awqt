<?php

require_once __DIR__ . "/../app.php";
require_once __DIR__ . "/authentication.php";

$__current_roles = null;

function authorization()
{
  global $__current_roles;

  if ($__current_roles !== null) {
    return $__current_roles;
  }

  $current_account = authentication();

  if ($current_account === false) {
    return [];
  }

  $account_id = (int) $current_account['id'];

  $current_roles = execute_sql("
    SELECT attribute
    FROM account_roles
    WHERE account_id = :account_id
  ", [
    ":account_id" => $account_id,
  ])->fetchAll();

  $__current_roles = $current_roles;

  return $current_roles;
}

function require_authorization()
{
  $current_roles = authorization();
  if (count($current_roles) === 0) {
    return send_json(401, ["ok" => false]);
  }
  return $current_roles;
}
