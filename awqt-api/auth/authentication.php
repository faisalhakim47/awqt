<?php

require_once __DIR__ . "/../app.php";

$__current_account = null;

function authentication()
{
  global $__current_account;

  if ($__current_account !== null) {
    return $__current_account;
  }

  $token = $_POST["token"];

  if (!isset($token) && strlen($token) < 32) {
    return false;
  }

  $current_account = execute_sql("
    SELECT
      accounts.id AS id,
      accounts.username AS username,
      accounts.email AS email
    FROM accounts
      JOIN account_sessions ON account_sessions.account_id = accounts.id
    WHERE account_sessions.id = :token
  ", [
    ":token" => [$token, PDO::PARAM_STR],
  ])->fetch();

  if (!$current_account) {
    return false;
  }

  $__current_account = $current_account;

  execute_sql("
    UPDATE account_sessions
    SET date_accessed = CURRENT_TIMESTAMP
    WHERE id = :token
  ", [
    ":token" => [$token, PDO::PARAM_STR],
  ])->fetch();

  return $current_account;
}

function require_authentication()
{
  $current_user = authentication();
  if ($current_user === false) {
    return send_json(401, ["ok" => false]);
  }
  return $current_user;
}
