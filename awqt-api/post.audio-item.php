<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/model/audio.php";

$filename = require_querystring("filename");

$audio = ensure_audio(
  $filename,
  "php://input"
);

send_json(200, $audio);
