<?php

require_once __DIR__ . "/app.php";

$prepare = get_query([
  "md5" => [
    "sql_query" => "audios.md5 = :md5",
    "param_type" => PDO::PARAM_STR,
    "required" => true,
  ],
]);

$result = execute_sql("
  SELECT *
  FROM audios
  {$prepare["sql_query"]}
  LIMIT 1
", $prepare["params"])->fetch();

send_json(200, $result);
