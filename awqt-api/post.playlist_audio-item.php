<?php

require_once __DIR__ . "/app.php";

$playlist_audio = require_json_data();

$result = execute_insert_sql("playlist_audios", [
  "playlist_id" => [$playlist_audio["playlist_id"], PDO::PARAM_STR],
  "audio_md5" => [$playlist_audio["audio_md5"], PDO::PARAM_STR],
]);

send_json(200, [
  "id" => $result,
]);
