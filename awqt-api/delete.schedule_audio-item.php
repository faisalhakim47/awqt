<?php

require_once __DIR__ . "/app.php";

$data = require_json_data();

$result = execute_delete_sql("schedule_audios", [
  "audio_md5" => [$data["audio_md5"], PDO::PARAM_STR],
  "schedule_id" => [$data["schedule_id"], PDO::PARAM_INT],
]);

send_json(200, $result);
