<?php

require_once __DIR__ . "/../app.php";

function compute_cron($cron)
{
  $year = (int) date("Y");
  $months = time_instance($cron["months"], range(1, 12));
  $days = time_instance($cron["days"], range(1, 7));
  $hours = time_instance($cron["hours"], range(0, 23));
  $minutes = time_instance($cron["minutes"], range(0, 59));
  $seconds = time_instance($cron["seconds"], range(0, 59));

  $computed_crons = [];

  foreach ($months as $month) {
    $dates = time_instance($cron["dates"], days_in_month($year, $month));
    if ($cron["days"] !== "*") {
      $dates = array_filter(
        $dates,
        function ($date) use ($year, $month, $days) {
          $time = strtotime($year . "-" . $month . "-" . $date);
          $day = ((int) date("w", $time)) + 1;
          return in_array($day, $days);
        }
      );
    }
    foreach ($dates as $date) {
      foreach ($hours as $hour) {
        foreach ($minutes as $minute) {
          foreach ($seconds as $second) {
            $time = "{$year}-{$month}-{$date} {$hour}:{$minute}:{$second}";
            array_push($computed_crons, [
              "cron_id" => [$cron["id"], PDO::PARAM_INT],
              "time" => [$time, PDO::PARAM_STR],
            ]);
          }
        }
      }
    }
  }

  return $computed_crons;
}

function time_instance($time, $all)
{
  return $time === "*" ? $all : split_to_number($time);
}

function split_to_number($string)
{
  return array_map(function ($date) {
    return (int) $date;
  }, explode(",", $string));
}
