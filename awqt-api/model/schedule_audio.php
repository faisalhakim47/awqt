<?php

require_once __DIR__ . "/../app.php";

function schedule_audio_sibling($direction, $schedule_id, $audio_md5)
{
  $subject = execute_sql("
    SELECT
      audio_md5,
      schedule_id,
      position
    FROM schedule_audios
    WHERE schedule_id = :schedule_id AND audio_md5 = :audio_md5
  ", [
    ":schedule_id" => [$schedule_id, PDO::PARAM_INT],
    ":audio_md5" => [$audio_md5, PDO::PARAM_STR],
  ])->fetch();
  if (!$subject) send_json(400, ["msg" => "Schedule audio not found."]);
  $comparator = $direction === 'prev' ? '<' : '>';
  $order_direction = $direction === 'prev' ? 'DESC' : 'ASC';
  return execute_sql("
    SELECT
      audio_md5,
      schedule_id,
      position
    FROM schedule_audios
    WHERE schedule_id = :schedule_id AND position $comparator :position
    ORDER BY position $order_direction
    LIMIT 1
  ", [
    ":schedule_id" => [$subject["schedule_id"], PDO::PARAM_INT],
    ":position" => [$subject["position"], PDO::PARAM_INT],
  ])->fetch();
}
