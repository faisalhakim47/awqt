<?php

function awqt_restart()
{
  return json_decode(file_get_contents('http://127.0.0.1:1111/restart'), true);
}

function awqt_update()
{
  return json_decode(file_get_contents('http://127.0.0.1:1111/update'), true);
}

function schedule_test()
{
  return json_decode(file_get_contents('http://127.0.0.1:5000/schedule_test'), true);
}

function schedule_next()
{
  return json_decode(file_get_contents('http://127.0.0.1:5000/schedule_next'), true);
}

function compute_next()
{
  return json_decode(file_get_contents('http://127.0.0.1:5000/compute_next'), true);
}

function active_jobs()
{
  return json_decode(file_get_contents('http://127.0.0.1:5000/active_jobs'), true);
}

function speaker_on()
{
  return json_decode(file_get_contents('http://127.0.0.1:5000/speaker_on'), true);
}

function speaker_off()
{
  return json_decode(file_get_contents('http://127.0.0.1:5000/speaker_off'), true);
}

function speaker_toggle() {
  return json_decode(file_get_contents('http://127.0.0.1:5000/speaker_toggle'), true);  
}

function audio_pause()
{
  return json_decode(file_get_contents('http://127.0.0.1:5000/audio_pause'), true);
}

function audio_stop()
{
  return json_decode(file_get_contents('http://127.0.0.1:5000/audio_pause'), true);
}

function pin_on($pin)
{
  return json_decode(file_get_contents("http://127.0.0.1:5000/pin/{$pin}/on"), true);
}

function pin_off($pin)
{
  return json_decode(file_get_contents("http://127.0.0.1:5000/pin/{$pin}/off"), true);
}

function pin_toggle($pin)
{
  return json_decode(file_get_contents("http://127.0.0.1:5000/pin/{$pin}/toggle"), true);
}
