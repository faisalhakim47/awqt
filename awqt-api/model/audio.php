<?php

require_once __DIR__ . "/../app.php";

function ensure_audio($filename, $data_url)
{
  $md5 = md5_file($data_url);

  $audio = execute_sql("
    SELECT *
    FROM audios
    WHERE md5 = :md5
  ", [
    ":md5" => [$md5, PDO::PARAM_STR],
  ])->fetch();

  if ($audio) return $audio;

  $audio_path = AUDIO_PATH . $md5;

  if (!file_exists($audio_path)) {
    file_put_contents($audio_path, file_get_contents($data_url));
  }

  $meta = array_reduce(
    array_filter(
      array_map(function ($info) {
        return explode(" : ", $info);
      }, explode("\n", shell_exec("mediainfo -f '" . $audio_path . "'"))),
      function ($info) {
        return count($info) === 2;
      }
    ),
    function ($result, $info) {
      return $result + [
        trim($info[0]) => trim($info[1]),
      ];
    },
    []
  );

  $audio = [
    "md5" => $md5,
    "filename" => $filename,
    "filetype" => $meta["Internet media type"],
    "duration" => $meta["Duration"],
  ];

  execute_insert_sql("audios", $audio);

  return $audio;
}
