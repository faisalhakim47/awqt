<?php

function days_in_month($year, $month) {
  return range(1, cal_days_in_month(CAL_GREGORIAN, $month, $year));
}
