<?php

function indonesian_date($timestamp = "", $date_format = "j F Y", $suffix = "")
{
  // l, j F Y | H:i
  if (trim($timestamp) == "") {
    $timestamp = time();
  } elseif (!ctype_digit($timestamp)) {
    $timestamp = strtotime($timestamp);
  }
  # remove S (st,nd,rd,th) there are no such things in indonesia :p
  $date_format = preg_replace("/S/", "", $date_format);
  $pattern = array(
    '/Mon[^day]/', '/Tue[^sday]/', '/Wed[^nesday]/', '/Thu[^rsday]/',
    '/Fri[^day]/', '/Sat[^urday]/', '/Sun[^day]/', '/Monday/', '/Tuesday/',
    '/Wednesday/', '/Thursday/', '/Friday/', '/Saturday/', '/Sunday/',
    '/Jan[^uary]/', '/Feb[^ruary]/', '/Mar[^ch]/', '/Apr[^il]/', '/May/',
    '/Jun[^e]/', '/Jul[^y]/', '/Aug[^ust]/', '/Sep[^tember]/', '/Oct[^ober]/',
    '/Nov[^ember]/', '/Dec[^ember]/', '/January/', '/February/', '/March/',
    '/April/', '/June/', '/July/', '/August/', '/September/', '/October/',
    '/November/', '/December/',
  );
  $replace = array("Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min",
    "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu",
    "Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des",
    "Januari", "Februari", "Maret", "April", "Juni", "Juli", "Agustus", "Sepember",
    "Oktober", "November", "Desember",
  );
  $date = date($date_format, $timestamp);
  $date = preg_replace($pattern, $replace, $date);
  $date = "{$date} {$suffix}";
  return $date;
}

function indonesian_address($jsonstring_address)
{
  if (!isset($jsonstring_address)) {
    return "";
  }

  $address = json_decode($jsonstring_address, true);
  $street = htmlspecialchars($address["street"]);
  $rt = htmlspecialchars($address["rt"]);
  $rw = htmlspecialchars($address["rw"]);
  $village = htmlspecialchars($address["village"]);
  $district = htmlspecialchars($address["district"]);
  $regency = htmlspecialchars($address["regency"]);
  $postalcode = htmlspecialchars($address["postalcode"]);
  return "{$street} RT {$rt} RW {$rw} Kel. {$village} Kec. {$district} {$regency} {$postalcode}";
}

function parse_nik($nik)
{
  $date = (int) substr($nik, 6, 2);
  $month = (int) substr($nik, 8, 2);
  $year = (int) substr($nik, 10, 2);
  $current_year = (int) date("y");

  if ($date < 40) {
    $sex = "Laki-laki";
  } else {
    $date = $date - 40;
    $sex = "Perempuan";
  }

  if ($current_year < $year) {
    $year = 1900 + $year;
  } else {
    $year = 2000 + $year;
  }

  $birth_date = date(
    "Y-m-d H:i:s",
    mktime(0, 0, 0, $month, $date, $year)
  );

  return [
    "sex" => $sex,
    "birth_date" => $birth_date,
  ];
}
