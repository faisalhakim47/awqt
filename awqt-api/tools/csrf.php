<?php

require_once __DIR__ . "/server.php";

if (array_search(false, array_map(function ($referer) {
  return strpos($_SERVER["HTTP_REFERER"], $referer) === 0;
}, $REFERER_WHITELIST)) === false) {
  $DISABLE_CSRF = true;
}

if (!DEVELOPMENT && !isset($DISABLE_CSRF)) {
  send_json(403, ["ok" => false]);
}
