<?php

require_once __DIR__ . "/app.php";

$prepare = get_query([
  "keyword" => [
    "sql_query" => "name LIKE :keyword",
    "param_type" => PDO::PARAM_STR,
  ],
]);

$result = execute_sql("
  SELECT id, name
  FROM schedules
  {$prepare["sql_query"]}
", $prepare["params"])->fetchAll();

send_json(200, $result);
