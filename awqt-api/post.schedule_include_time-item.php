<?php

require_once __DIR__ . "/app.php";

$data = require_json_data();

$include_time = [
  "schedule_id" => [$data["schedule_id"], PDO::PARAM_INT],
];

if (is_int($data["cron_id"])) {
  $include_time += [
    "cron_id" => [$data["cron_id"], PDO::PARAM_INT],
  ];
} else {
  $include_time += [
    "time_id" => [$data["time_id"], PDO::PARAM_INT],
  ];
}

$include_time["id"] = execute_insert_sql("schedule_include_times", $include_time);

send_json(200, $include_time);
