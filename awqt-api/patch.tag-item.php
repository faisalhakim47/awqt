<?php

require_once __DIR__ . "/app.php";

$tag = require_json_data();

$result = execute_update_sql("tags", [
  "is_active" => [$tag["is_active"], PDO::PARAM_INT],
], [
  "name" => [$tag["name"], PDO::PARAM_STR],
]);

send_json(200, $result);
