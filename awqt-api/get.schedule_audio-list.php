<?php

require_once __DIR__ . "/app.php";

$prepare = get_query([
  "schedule_id" => [
    "sql_query" => "schedule_audios.schedule_id = :schedule_id",
    "param_type" => PDO::PARAM_INT,
  ],
]);

$result = execute_sql("
  SELECT
    schedule_audios.schedule_id AS schedule_id,
    schedule_audios.position AS position,
    audios.md5 AS md5,
    audios.filename AS filename,
    audios.filetype AS filetype,
    audios.duration AS duration
  FROM schedule_audios
  JOIN audios ON audios.md5 = schedule_audios.audio_md5
  {$prepare["sql_query"]}
  ORDER BY schedule_audios.position
", $prepare["params"])->fetchAll();

send_json(200, $result);
