<?php

require_once __DIR__ . "/app.php";
require_once __DIR__ . "/model/awqt.php";

$result = awqt_update();

send_json(200, [
  "ok" => true,
  "result" => $result,
]);
