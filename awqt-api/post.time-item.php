<?php

require_once __DIR__ . "/app.php";

$time = require_json_data();

$time["id"] = execute_insert_sql("times", [
  "name" => [$time["name"], PDO::PARAM_STR],
]);

send_json(200, $time);
