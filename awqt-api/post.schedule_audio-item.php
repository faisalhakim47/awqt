<?php

require_once __DIR__ . "/app.php";

$data = require_json_data();

$last_schedule_audio = execute_sql("
  SELECT position
  FROM schedule_audios
  WHERE schedule_id = :schedule_id
  ORDER BY position DESC
  LIMIT 1
", [
  ":schedule_id" => [$data["schedule_id"], PDO::PARAM_INT],
])->fetch();

$position = $last_schedule_audio ? ((int) $last_schedule_audio["position"]) + 1 : 1;

$schedule_audio_id = execute_insert_sql("schedule_audios", [
  "schedule_id" => [$data["schedule_id"], PDO::PARAM_INT],
  "audio_md5" => [$data["audio_md5"], PDO::PARAM_STR],
  "position" => [$position, PDO::PARAM_INT],
]);

send_json(200, [
  "schedule_audio_id" => $schedule_audio_id,
]);
