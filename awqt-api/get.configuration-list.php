<?php

require_once __DIR__ . "/app.php";

$prepare = get_query([
  "names" => [
    "sql_query" => "name IN :names",
    "param_type" => PDO::PARAM_STR,
    "map" => function ($names) {
      return "($names)";
    },
  ],
]);

$names = explode(",", get_querystring("names") ?: "");

$configs = execute_sql("
  SELECT name, value
  FROM configurations
  {$prepare["sql_query"]}
", $prepare["params"])->fetchAll();

send_json(200, $configs);
