<?php

require_once __DIR__ . "/app.php";

$schedule_id = require_querystring("schedule_id");

$result = execute_sql("
  SELECT id, schedule_id, time_id, cron_id
  FROM schedule_include_times
  WHERE schedule_id = :schedule_id
", [
  ":schedule_id" => [$schedule_id, PDO::PARAM_STR],
])->fetchAll();

send_json(200, $result);
